<?php

namespace Empu\BackendAdditive\Classes;

use Illuminate\Support\Carbon;

class ListColumnTypes
{
    use \October\Rain\Extension\ExtendableTrait;

    public static function timeDiff($value, $column, $record)
    {
        $against = isset($column->against) ? $record->against : null;

        return $value->diffInHours($against) > 22
            ? $value->format('d M Y')
            : $value->diffForHumans($against);
    }

    public static function softdeleteStatus($value, $column, $record)
    {
        $status = isset($column->config['status']) ? $column->config['status'] : ['Live', 'Archived'];

        return is_null($value) ? $status[0] : $status[1];
    }
}
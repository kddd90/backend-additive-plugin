<?php

namespace Empu\BackendAdditive\Behaviors;

use Backend\Classes\ControllerBehavior;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use October\Rain\Exception\ApplicationException;
use October\Rain\Support\Facades\Flash;

class RestoreableList extends ControllerBehavior
{
    /**
     * @var array List definitions, keys for alias and value for configuration.
     */
    protected $listDefinitions;

    /**
     * @var string The primary list alias to use. Default: list
     */
    protected $primaryDefinition;

    /**
     * @var array Configuration values that must exist when applying the primary config file.
     * - modelClass: Class name for the model
     * - list: List column definitions
     */
    protected $requiredConfig = ['modelClass', 'list'];

    /**
     * @var array List configuration, keys for alias and value for config objects.
     */
    protected $listConfig = [];

    public function __construct($controller)
    {
        parent::__construct($controller);

        $satisfied =
            $this->controller->isClassExtendedWith('Backend.Behaviors.ListController');

        // Throw errof if behaviors if not already implemented
        if (! $satisfied) {
            throw new ApplicationException('ListController must be implemented in the controller.');
        }

        /*
         * Extract list definitions
         */
        if (is_array($controller->listConfig)) {
            $this->listDefinitions = $controller->listConfig;
            $this->primaryDefinition = key($this->listDefinitions);
        }
        else {
            $this->listDefinitions = ['list' => $controller->listConfig];
            $this->primaryDefinition = 'list';
        }

        /*
         * Build configuration
         */
        $this->setConfig($this->listDefinitions[$this->primaryDefinition], $this->requiredConfig);
    }

    /**
     * Bulk restore records.
     * @return void
     * @throws \October\Rain\Exception\ApplicationException when the parent definition is missing.
     */
    public function index_onRestore()
    {
        if (method_exists($this->controller, 'onRestore')) {
            return call_user_func_array([$this->controller, 'onRestore'], func_get_args());
        }

        /*
         * Validate checked identifiers
         */
        $checkedIds = post('checked');

        if (!$checkedIds || !is_array($checkedIds) || !count($checkedIds)) {
            Flash::error(Lang::get('empu.backendadditive::lang.list.restore_selected_empty'));
            return $this->controller->listRefresh();
        }

        /*
         * Establish the list definition
         */
        $definition = post('definition', $this->primaryDefinition);

        if (!isset($this->listDefinitions[$definition])) {
            throw new ApplicationException(Lang::get('backend::lang.list.missing_parent_definition', compact('definition')));
        }

        $listConfig = $this->controller->listGetConfig($definition);

        /*
         * Create the model
         */
        $class = $listConfig->modelClass;
        $model = new $class;
        $model = $this->controller->listExtendModel($model, $definition);

        /*
         * Create the query
         */
        $query = $model->newQuery();
        $this->controller->listExtendQueryBefore($query, $definition);

        $query->whereIn($model->getKeyName(), $checkedIds);
        $this->controller->listExtendQuery($query, $definition);

        /*
         * Get deleted records
         */
        $records = $query->onlyTrashed()->get();

        if ($records->count()) {
            foreach ($records as $record) {
                $record->restore();
            }

            Flash::success(Lang::get('empu.backendadditive::lang.list.restore_selected_success'));
        }
        else {
            Flash::error(Lang::get('empu.backendadditive::lang.list.restore_selected_empty'));
        }

        return $this->controller->listRefresh($definition);
    }

    public function onRestoreItem()
    {
        $satisfied =
            $this->controller->isClassExtendedWith('Backend.Behaviors.FormController')
            || $this->controller->isClassExtendedWith(PopupFormController::class);

        // Throw errof if behaviors if not already implemented
        if (! $satisfied) {
            throw new ApplicationException('FormController must be implemented in the controller.');
        }

        $manageId = post('manage_id');
        $record = $this->controller->formFindModelObject($manageId);

        try {
            $record->restore();
            Flash::success('Record berhasil dipulihkan.');
        } catch (\Throwable $th) {
            Flash::error($th->getMessage());
        }

        return Redirect::refresh();
    }
}
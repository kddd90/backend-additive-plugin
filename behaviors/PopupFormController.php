<?php

namespace Empu\BackendAdditive\Behaviors;

use Backend\Behaviors\FormController;
use Backend\Facades\Backend;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use October\Rain\Exception\AjaxException;

class PopupFormController extends FormController
{
    const SIZE_GIANT = 'giant';
    const SIZE_HUGE = 'huge';
    const SIZE_LARGE = 'large';
    const SIZE_SMALL = 'small';
    const SIZE_TINY = 'tiny';

    use \Empu\BackendAdditive\Traits\Popup {
        \Empu\BackendAdditive\Traits\Popup::popupMakePartial as popupFormMakePartial;
        \Empu\BackendAdditive\Traits\Popup::popupRender as popupFormRender;
    }

    public function popupFormButton(string $label, ?string $context = null, array $data = [], ?string $size = null): string
    {
        $data['popupform_context'] = $context = $context ?? FormController::CONTEXT_CREATE;
        $extraData = collect($data)->map(function($value, $key) {
            return "{$key}: '{$value}'";
        })->join(', ');

        return $this->popupMakePartial('button_' . $context, [
            'label' => $label,
            'extraData' => '{' . $extraData . '}',
            'size' => $size ?: self::SIZE_LARGE,
        ]);
    }

    /**
     * Accepted redirect value
     * `path/to/:manage_id` to redirect to desired page
     * `false` to keep on current page
     *
     * @var $actionRedirects
     * public $actionRedirects = ['create' => 'path/to/'];
     */

    public function onPopupForm()
    {
        try {
            $this->initPopupForm();
        }
        catch (\Exception $ex) {
            Log::error($ex);
            throw new AjaxException(['error' => $ex->getMessage()]);
        }

        $this->vars['popupFormTitle'] = $this->getConfig("{$this->context}[title]");

        return $this->popupFormRender(['popupHasFooter' => true]);
    }

    public function initPopupForm()
    {
        $manageId = post('manage_id');

        if (empty($manageId)) {
            $context = post('popupform_context', FormController::CONTEXT_CREATE);
            $model = $this->controller->formCreateModelObject();
            $model = $this->controller->formExtendModel($model) ?: $model;
        }
        else {
            $context = post('popupform_context', FormController::CONTEXT_UPDATE);
            $model = $this->controller->formFindModelObject($manageId);
        }

        $this->initForm($model, $context);
    }

    public function onCreateViaPopup()
    {
        $context = $this->popupFormGetContext();
        $defaultRedirect = $this->create_onSave($context);

        return $this->redirectTo($context, $defaultRedirect);
    }

    public function onUpdateViaPopup()
    {
        $manageId = post('manage_id');
        $context = $this->popupFormGetContext();
        $defaultRedirect = $this->update_onSave($manageId, $context);

        return $this->redirectTo($context, $defaultRedirect);
    }

    public function onDeleteViaPopup()
    {
        $manageId = post('manage_id');
        $context = $this->popupFormGetContext();
        $defaultRedirect = $this->update_onDelete($manageId, $context);

        return $this->redirectTo($context, $defaultRedirect);
    }

    protected function redirectTo(string $context, $defaultRedirect)
    {
        if (property_exists($this->controller, 'actionRedirects')) {
            $redirectPath = array_get($this->controller->actionRedirects, $context);

            if ($redirectPath === false) {
                return;
            }

            $manageId = post('manage_id');
            $redirectUrl = Backend::url($redirectPath);
            $redirectUrl = str_replace(':manage_id', $manageId, $redirectUrl);

            return Redirect::to($redirectUrl);
        }

        return $defaultRedirect;
    }

    public function popupFormGetContext()
    {
        $defaultContext = post('manage_id')
            ? FormController::CONTEXT_UPDATE
            : FormController::CONTEXT_CREATE;

        return post('popupform_context', $defaultContext);
    }
}
<?php

namespace Empu\BackendAdditive\Behaviors;

use Backend\Classes\ControllerBehavior;
use Backend\Facades\Backend;
use Exception;
use Maatwebsite\Excel\Facades\Excel;
use October\Rain\Support\Facades\Flash;

class ImportExcel extends ControllerBehavior
{
    public function import()
    {
        $this->controller->pageTitle = 'Import Excel';
    }

    public function onGetImportTemplate()
    {
        return [
            'url' => Backend::url('/import-template'),
        ];
    }

    public function onPopupImport()
    {
        return $this->makePartial('$/empu/backendadditive/behaviors/importexcel/partials/_popup_import.htm');
    }

    public function onImport($recordId)
    {
        if (!request()->has('file_import')) {
            Flash::error('Silakan pilih berkas berformat excel yang akan diimport!');
            return;
        }

        try {
            Excel::import($this->controller->excelImporter($recordId), request()->file('file_import'));

            Flash::success('Berhasil impor data.');
        }
        catch (\Maatwebsite\Excel\Validators\ValidationException $ex) {
            $failures = collect($ex->failures())
                ->mapToGroups(function ($failure) {
                    return [$failure->row() => [
                        'attribute' => $failure->attribute(),
                        'value' => $this->getFailureValue($failure),
                        'errors' => implode('; ', $failure->errors()),
                    ]];
                })->sortKeys();
            return [
                '#import-alert' => $this->makePartial(
                    '$/empu/backendadditive/behaviors/importexcel/partials/_import_alert.htm', [
                        'failures' => $failures,
                    ]
                ),
            ];
        }
        catch (Exception $ex) {
            Flash::error($ex->getMessage());
        }
    }

    protected function getFailureValue($failure)
    {
        $attribute = $failure->attribute();
        $values = $failure->values();

        return $values[$attribute] ?? null;
    }
}

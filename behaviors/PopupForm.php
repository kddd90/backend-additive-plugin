<?php

namespace Empu\BackendAdditive\Behaviors;

use Backend\Behaviors\FormController;
use Backend\Classes\ControllerBehavior;
use Illuminate\Support\Facades\Log;
use October\Rain\Exception\AjaxException;
use October\Rain\Exception\ApplicationException;

/**
 * Popup form
 * @deprecated v0.2.0
 */
class PopupForm extends ControllerBehavior
{
    use \Empu\BackendAdditive\Traits\Popup;

    public function __construct($controller)
    {
        parent::__construct($controller);

        $satisfied =
            $this->controller->isClassExtendedWith('Backend.Behaviors.FormController');

        // Throw errof if behaviors if not already implemented
        if (! $satisfied) {
            throw new ApplicationException('FormController must be implemented in the controller.');
        }
    }

    public function onPopupForm()
    {
        $manageId = post('manage_id');

        try {
            $form = $this->controller->asExtension('FormController');

            if (empty($manageId)) {
                $context = post('manage_context', FormController::CONTEXT_CREATE);
                $model = $this->controller->formCreateModelObject();
                $model = $this->controller->formExtendModel($model) ?: $model;
            }
            else {
                $context = post('manage_context', FormController::CONTEXT_UPDATE);
                $model = $this->controller->formFindModelObject($manageId);
            }

            $form->initForm($model, $context);
        }
        catch (\Exception $ex) {
            Log::error($ex);

            throw new AjaxException(['error' => 'Error occured during operations.']);
        }

        return $this->makePopupPartial(
            $form->getConfig("{$context}[title]"),
            [
                'body' => '$/empu/backendadditive/behaviors/popupform/partials/_form.htm',
                'footer' => '$/empu/backendadditive/behaviors/popupform/partials/_actions.htm',
            ]
        );
    }

    public function onCreate()
    {
        return $this->controller->asExtension('FormController')->create_onSave();
    }

    public function onUpdate()
    {
        $manageId = post('manage_id');

        return $this->controller->asExtension('FormController')->update_onSave($manageId);
    }

    public function onDelete()
    {
        $manageId = post('manage_id');

        return $this->controller->asExtension('FormController')->update_onDelete($manageId);
    }
}
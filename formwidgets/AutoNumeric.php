<?php

namespace Empu\BackendAdditive\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Illuminate\Support\Carbon;
use October\Rain\Support\Str;

/**
 * Auto Numeric Form Widget
 */
class AutoNumeric extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'empu_backendadditive_autonumeric';

    public $format = [];

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig(['format']);
    }

    protected function formatToAttributes()
    {
        $allowedSettings = [
            'aSep', 'dGroup', 'aDec', 'altDec', 'aSign', 'pSign', 'vMin', 'vMax', 'mDec', 'mRound',
            'aPad', 'nBracket', 'wEmpty', 'lZero', 'aForm', 'anDefault'
        ];
        $attributes = [];

        foreach ($this->format as $key => $value) {
            if (! in_array($key, $allowedSettings)) {
                continue;
            }

            $key = 'data-'.Str::kebab($key);
            $attributes[$key] = $value;
        }

        return $attributes;
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();

        $this->formField->attributes($this->formatToAttributes());

        return $this->makePartial('autonumeric');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['field'] = $this->formField;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addJs('js/autonumeric.js', 'Empu.BackendAdditive');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        $strNumber = preg_replace('~[^0-9\.]~', '', $value);

        return floatval($strNumber);
    }
}

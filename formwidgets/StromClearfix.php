<?php

namespace Empu\BackendAdditive\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * StromClearfix Form Widget
 */
class StromClearfix extends FormWidgetBase
{
    public $cssClass = '';

    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'empu_backendadditive_strom_clearfix';

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig(['cssClass']);
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        return $this->makePartial('stromclearfix');
    }
}

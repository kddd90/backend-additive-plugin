<?php

namespace Empu\BackendAdditive\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * Static text form widget
 */
class StaticText extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'empu_backendadditive_plaintext';

    public $weight = 'normal';

    public $emptyText = '';

    public $align = 'left';

    public $multiline = false;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->fillFromConfig(['weight', 'emptyText', 'align', 'multiline']);
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/statictext.css', 'Empu.BackendAdditive');
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->vars['value'] = $this->getLoadValue();

        return $this->makePartial('control');
    }
}

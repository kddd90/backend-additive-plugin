<?php namespace Empu\BackendAdditive\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * RemoteDropdown Form Widget
 */
class RemoteDropdown extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'empu_backendadditive_remote_dropdown';

    public $settings = [];

    /**
     * @inheritDoc
     */
    public function init()
    {
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();

        return $this->makePartial('remotedropdown');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['field'] = $this->formField;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        //
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }
}

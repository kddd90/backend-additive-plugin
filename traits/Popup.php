<?php

namespace Empu\BackendAdditive\Traits;

use October\Rain\Exception\SystemException;

/**
 * Popup controller trait
 */
trait Popup
{
    public function popupMakePartial(string $partial, array $params = []): string
    {
        $contents = $this->controller->makePartial('popup_'.$partial, $params + $this->vars, false);

        if (!$contents) {
            $contents = $this->makePartial($partial, $params);
        }

        return $contents;
    }

    public function popupRender(array $params = []): string
    {
        return $this->popupMakePartial('container', $params);
    }

    /**
     * Deprecated
     *
     * @param string $title
     * @param string|array $partials
     * @param array $vars
     * @return string
     * @deprecated v0.6.0
     */
    public function makePopupPartial($title, $partials, $vars = [])
    {
        $this->vars['title'] = $title;
        $this->vars['vars'] = $vars;

        if (is_array($partials)) {
            if (empty($partials['body'])) {
                throw new SystemException('Body partial is not defined!');
            }

            $this->vars['partialBody'] = $partials['body'];
            $this->vars['partialFooter'] = isset($partials['footer']) ? $partials['footer'] : false;
        }
        else {
            $this->vars['partialBody'] = $partials;
            $this->vars['partialFooter'] = false;
        }

        return $this->makePartial('$/empu/backendadditive/views/modal.htm');
    }
}

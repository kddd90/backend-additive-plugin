<?php

namespace Empu\BackendAdditive\Traits;

/**
 * Custom layout
 */
trait CustomLayout
{
    /**
     * Use custom layout from module
     *
     * @param string $module 'Vendor.ModuleName'
     * @return void
     */
    public function useCustomLayouts($module)
    {
        $path = str_replace('.', '/', strtolower($module));
        $this->layoutPath[] = '~/plugins/' . $path . '/layouts';
    }
}
